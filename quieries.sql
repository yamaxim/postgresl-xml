# Import tags
CREATE TABLE tags AS
SELECT
(xpath('/row/@Id', x))[1]::text::int as id,
(xpath('/row/@TagName', x))[1]::text as tagname,
(xpath('/row/@Count', x))[1]::text::int as count,
(xpath('/row/@ExcerptPostId', x))[1]::text::int as excerptpostid,
(xpath('/row/@WikiPostId', x))[1]::text::int as wikipostid
from unnest(xpath('/tags/row', pg_read_file('Tags.xml', 3, 10000000)::xml)) x;

# Query to test index
SELECT excerptpostid, count(excerptpostid) as cnt FROM tags
GROUP BY excerptpostid
HAVING excerptpostid > 100 AND excerptpostid < 1000
LIMIT 3;

# Create index
CREATE INDEX excerpt_index ON tags (excerptpostid);