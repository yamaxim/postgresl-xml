docker-compose up -d<br>


## Call PHP
docker-compose exec php bash<br>

## Call Postgresql
docker-compose exec postgres bash<br>
cp /var/local/data/Tags.xml /var/lib/postgresql/data<br>
psql -U postgres postgres<br>

docker-compose exec php-fpm composer install<br>
docker-compose exec php-fpm php cli/generate.php<br>