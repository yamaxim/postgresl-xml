<?php

require_once './vendor/autoload.php';

$connection = pg_connect("
host=postgres 
port=5432 
dbname=postgres 
user=postgres 
password=postgres");

if (!$connection) {
    echo "Произошла ошибка connect.\n";
    exit;
}

$faker = Faker\Factory::create();

$batch = "";
for($i = 0; $i < 1000000; $i++){

    $batch .= "
    INSERT INTO tags VALUES
    (random(), '" . pg_escape_string($faker->name) . "', random(), random(), random());";

    if($i % 1000 == 0){
        $result = pg_query($connection, $batch);
        if (!$result) {
            echo "Произошла ошибка query {$i}.\n";
            exit;
        }
        $batch = "";
    }
}

$result = pg_query($connection, "SELECT count(Id) FROM tags");
if (!$result) {
    echo "Произошла ошибка query.\n";
    exit;
} else {
    $row = pg_fetch_row($result);
}

echo "complete! {$row[0]} tags now." . PHP_EOL;